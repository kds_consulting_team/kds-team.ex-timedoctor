import logging
import os
import sys
from kbc.client_base import HttpClientBase

BASE_URL = 'https://webapi.timedoctor.com/v1.1/'
DEFAULT_LIMIT = 500


class TimeDoctorClient(HttpClientBase):

    def __init__(self, refreshToken, clientId, clientSecret):

        self.paramRefreshToken = refreshToken
        self.paramClientId = clientId
        self.paramClientSecret = clientSecret

        paramsDefault = {
            '_format': 'json'
        }

        super().__init__(BASE_URL, default_params=paramsDefault)

    def refreshAccessToken(self):

        urlRefresh = 'https://webapi.timedoctor.com/oauth/v2/token'
        paramsRefresh = {
            'client_id': self.paramClientId,
            'client_secret': self.paramClientSecret,
            'refresh_token': self.paramRefreshToken,
            'grant_type': 'refresh_token'
        }

        reqRefresh = self.get_raw(urlRefresh, params=paramsRefresh)
        scRefresh, jsRefresh = reqRefresh.status_code, reqRefresh.json()

        if scRefresh == 200:

            logging.info("Access token refreshed.")
            self.paramAccessToken = jsRefresh['access_token']
            self.paramRefreshToken = jsRefresh['refresh_token']

            self._auth_header = {
                'Authorization': f'Bearer {self.paramAccessToken}'
            }

            return jsRefresh

        else:

            logging.error("Could not refresh access token.")
            logging.error("Try re-authorizing the extractor! \nReceived: %s - %s." % (scRefresh, jsRefresh))

            sys.exit(1)

    def getCompanies(self):

        urlCompanies = os.path.join(self.base_url, 'companies')

        reqCompanies = self.get_raw(urlCompanies)
        scCompanies, jsCompanies = reqCompanies.status_code, reqCompanies.json()

        if scCompanies != 200:

            logging.error('\n'.join(["Could not obtain a list of companies.",
                                     f"Received: {scCompanies} - {jsCompanies}."]))
            sys.exit(1)

        else:

            return jsCompanies['accounts']

    def getUsers(self, companyId):

        urlUsers = os.path.join(self.base_url, f'companies/{companyId}/users')

        reqUsers = self.get_raw(urlUsers)
        scUsers, jsUsers = reqUsers.status_code, reqUsers.json()

        if scUsers != 200:

            logging.error(f"Could not obtain users for company {companyId}.")
            logging.error(f"Received: {scUsers} - {jsUsers}.")
            sys.exit(1)

        else:
            return jsUsers['users']

    def getUserProjects(self, companyId, userId):

        offset = 1
        moreRecords = True
        allProjects = []

        urlProjects = os.path.join(self.base_url, f'companies/{companyId}/users/{userId}/projects')

        while moreRecords is True:

            paramsProjects = {
                'offset': offset,
                'limit': DEFAULT_LIMIT,
                'all': 1,
                'assigned': 0
            }

            reqProjects = self.get_raw(urlProjects, params=paramsProjects)
            scProjects, jsProjects = reqProjects.status_code, reqProjects.json()

            if scProjects == 200:

                allProjects += jsProjects['projects']

                if jsProjects['count'] == DEFAULT_LIMIT:
                    offset += DEFAULT_LIMIT

                else:
                    moreRecords = False

            elif scProjects == 403:
                logging.warning(f"Warning Could not obtain projects for user {userId}.")
                moreRecords = False

            else:

                logging.error(f"Could not obtain projects for user {userId}.")
                logging.error(f"Resulted in {scProjects} Error")
                logging.info(jsProjects)
                sys.exit(1)

        return allProjects

    def getUserTasks(self, companyId, userId):

        offset = 1
        moreRecords = True
        allTasks = []

        urlTasks = os.path.join(self.base_url, f'companies/{companyId}/users/{userId}/tasks')

        while moreRecords is True:

            paramsTasks = {
                'offset': offset,
                'limit': DEFAULT_LIMIT,
                'status': 'all'
            }

            reqTasks = self.get_raw(urlTasks, params=paramsTasks)
            scTasks, jsTasks = reqTasks.status_code, reqTasks.json()

            if scTasks == 200:

                allTasks += jsTasks['tasks']

                if jsTasks['count'] == DEFAULT_LIMIT:
                    offset += DEFAULT_LIMIT

                else:
                    moreRecords = False

            else:

                logging.error("Could not obtain projects for user %s." % userId)
                logging.error(f"Received: {scTasks} - {jsTasks}.")
                sys.exit(1)

        return allTasks

    def getWorklogs(self, companyId, startDate, endDate):

        offset = 1
        moreRecords = True
        allWorklogs = []

        urlWorklogs = os.path.join(self.base_url, f'companies/{companyId}/worklogs')

        while moreRecords is True:

            paramsWorklogs = {
                'offset': offset,
                'limit': DEFAULT_LIMIT,
                'consolidated': 0,
                'start_date': startDate,
                'end_date': endDate
            }

            reqWorklogs = self.get_raw(urlWorklogs, params=paramsWorklogs)
            scWorklogs, jsWorklogs = reqWorklogs.status_code, reqWorklogs.json()

            if scWorklogs == 200:

                worklogs = jsWorklogs['worklogs']['items']

                allWorklogs += worklogs

                if len(worklogs) == DEFAULT_LIMIT:
                    offset += DEFAULT_LIMIT

                else:
                    moreRecords = False

            else:

                logging.error("Could not obtain worklogs.")
                logging.error(f"Received: {scWorklogs} - {jsWorklogs}.")
                sys.exit(1)

        return allWorklogs

    def getPayrolls(self, companyId):

        offset = 1
        moreRecords = True
        allPayrolls = []

        urlPayrolls = os.path.join(self.base_url, f'companies/{companyId}/payrolls')

        while moreRecords is True:

            paramsPayrolls = {
                'offset': offset,
                'limit': DEFAULT_LIMIT
            }

            reqPayrolls = self.get_raw(urlPayrolls, params=paramsPayrolls)
            scPayrolls, jsPayrolls = reqPayrolls.status_code, reqPayrolls.json()

            if scPayrolls == 200:

                allPayrolls += jsPayrolls

                if len(jsPayrolls) == DEFAULT_LIMIT:
                    offset += DEFAULT_LIMIT

                else:
                    moreRecords = False

            else:

                logging.error("Could not obtain worklogs.")
                logging.error(f"Received: {scPayrolls} - {jsPayrolls}.")
                sys.exit(1)

        return allPayrolls

    def getPoortime(self, companyId, startDate, endDate):

        urlPoortime = os.path.join(self.base_url, f'companies/{companyId}/poortime')
        paramsPoortime = {
            'start_date': startDate,
            'end_date': endDate
        }

        reqPoortime = self.get_raw(urlPoortime, params=paramsPoortime)
        scPoortime, jsPoortime = reqPoortime.status_code, reqPoortime.json()

        if scPoortime == 200:
            return jsPoortime

        else:
            logging.error("There was an error downloading poortime.")
            sys.exit(1)

    def getWebAndApps(self, companyId, startDate, endDate, users):

        urlWebAndApps = os.path.join(self.base_url, f'companies/{companyId}/webandapp')
        paramsWebAndApps = {
            'start_date': startDate,
            'end_date': endDate,
            'user_id': users
        }

        reqWebAndApps = self.get_raw(urlWebAndApps, params=paramsWebAndApps)
        scWebAndApps, jsWebAndApps = reqWebAndApps.status_code, reqWebAndApps.json()

        if scWebAndApps == 200:
            return jsWebAndApps

        else:
            logging.error(f"Could not download data about web and applications for users: {users}.")
            logging.error(f"Received: {scWebAndApps} - {jsWebAndApps}.")
            sys.exit(1)
