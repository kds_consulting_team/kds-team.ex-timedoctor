import dateparser
import json
import logging
import sys
from kbc.env_handler import KBCEnvHandler
from timedoctor.client import TimeDoctorClient
from timedoctor.result import TimeDoctorWriter, TimeDoctorWriterWAA


KEY_ENDPOINTS = 'endpoints'
KEY_DATESETTINGS = 'date_settings'
KEY_INCREMENTAL = 'incremental'
MANDATORY_PARAMETERS = [KEY_ENDPOINTS]

KEY_DATESETTINGS_START = 'start_time'
KEY_DATESETTINGS_END = 'end_time'

AUTH_KEY_REFRESHTOKEN = 'refresh_token'
AUTH_KEY_CLIENTID = 'appKey'
AUTH_KEY_CLIENTSECRET = '#appSecret'

SUPPORTED_ENDPOINTS = ['projects', 'tasks', 'worklogs', 'webandapp', 'users', 'companies']


class TimeDoctor(KBCEnvHandler):

    def __init__(self):

        super().__init__(MANDATORY_PARAMETERS)

        self.validate_config(MANDATORY_PARAMETERS)

        self.paramEndpoints = self.cfg_params[KEY_ENDPOINTS]
        _date_settings = self.cfg_params.get(KEY_DATESETTINGS, {})
        self.paramStartDate = _date_settings.get(KEY_DATESETTINGS_START, '30 days ago')
        self.paramEndDate = _date_settings.get(KEY_DATESETTINGS_END, 'today')
        self.paramIncremental = self.cfg_params.get(KEY_INCREMENTAL, True)

        self._checkEndpoints()
        self.createWriters()
        self.getDates()

        self.initializeClient()

    def initializeClient(self):

        self.paramAuthentication = self.getLatestAuthorization()

        refreshToken = json.loads(self.paramAuthentication['#data'])[AUTH_KEY_REFRESHTOKEN]
        clientId = self.paramAuthentication[AUTH_KEY_CLIENTID]
        clientSecret = self.paramAuthentication[AUTH_KEY_CLIENTSECRET]

        self.client = TimeDoctorClient(refreshToken, clientId, clientSecret)
        self.paramAuthentication['#data'] = json.dumps(self.client.refreshAccessToken())
        self.write_state_file(self.paramAuthentication)

    def _checkEndpoints(self):

        diffEndpoints = set(list(self.paramEndpoints)) - set(SUPPORTED_ENDPOINTS)

        if len(diffEndpoints) > 0:
            logging.error(f"Unsupported endpoints {diffEndpoints} specified.")
            sys.exit(1)

    def getAuthorization(self):

        return self.configuration.config_data.get('authorization', {}).get('oauth_api', {}).get('credentials', {})

    def getLatestAuthorization(self):

        configAuth = self.getAuthorization()
        stateAuth = self.get_state_file()

        if stateAuth is None and configAuth != {}:
            return configAuth

        elif configAuth == {}:
            logging.error("No authorization provided in the configuration.")
            sys.exit(1)

        comparisonList = [
            configAuth.get('id') == stateAuth.get('id'),
            configAuth.get('authorizedFor') == stateAuth.get('authorizedFor'),
            configAuth.get('creator') == stateAuth.get('creator'),
            configAuth.get('created') == stateAuth.get('created'),
            configAuth.get('appKey') == stateAuth.get('appKey'),
            configAuth.get('#appSecret') == stateAuth.get('#appSecret')
        ]

        if all(comparisonList) is True:
            logging.info("Using authentication from state file.")
            return stateAuth

        else:
            logging.info("Using authentication from configuration.")
            return configAuth

    def getDates(self):

        start_date = dateparser.parse(self.paramStartDate)
        end_date = dateparser.parse(self.paramEndDate)

        if start_date is None or end_date is None:
            logging.error(
                f"Incorrect format provided for start ({self.paramStartDate}) or end ({self.paramEndDate}) date.")
            sys.exit(1)

        else:
            self.paramDates = {'start': start_date.strftime('%Y-%m-%d'),
                               'end': end_date.strftime('%Y-%m-%d')}

            self.paramChunkDates = self.split_dates_to_chunks(start_date, end_date, 60, '%Y-%m-%d')

    def createWriters(self):

        if 'users' in self.paramEndpoints:
            self.wrUsers = TimeDoctorWriter('users', self.paramIncremental, self.data_path)

        if 'companies' in self.paramEndpoints:
            self.wrCompanies = TimeDoctorWriter('companies', self.paramIncremental, self.data_path)

        if 'projects' in self.paramEndpoints:
            self.wrProjects = TimeDoctorWriter('projects', self.paramIncremental, self.data_path)

        if 'tasks' in self.paramEndpoints:
            self.wrTasks = TimeDoctorWriter('tasks', self.paramIncremental, self.data_path)

        if 'worklogs' in self.paramEndpoints:
            self.wrWorklogs = TimeDoctorWriter('worklogs', self.paramIncremental, self.data_path)

        if 'webandapp' in self.paramEndpoints:
            self.wrWebAndApps = TimeDoctorWriterWAA(self.paramIncremental, dataPath=self.data_path)

    def run(self):

        logging.info("Downloading companies.")
        _allCompanies = self.client.getCompanies()
        self.respCompanies = {str(a['company_id']): {'name': a['company_name']} for a in _allCompanies}

        if 'companies' in self.paramEndpoints:
            self.wrCompanies.writerow(_allCompanies)

        logging.info("Downloading users.")
        _allUsers = []
        for _cmp in self.respCompanies:

            _allUsers += self.client.getUsers(_cmp)

        self.respUsers = {str(u['user_id']): {'name': u['full_name'],
                                              'company_id': str(u['company_id'])} for u in _allUsers}
        if 'users' in self.paramEndpoints:
            self.wrUsers.writerow(_allUsers)

        if 'projects' in self.paramEndpoints:
            logging.info("Downloading projects.")
            _allProjects = []

            for _user, _attr in self.respUsers.items():
                _allProjects += self.client.getUserProjects(_attr['company_id'], _user)

            self.wrProjects.writerow(_allProjects)

        if 'tasks' in self.paramEndpoints:
            logging.info("Downloading tasks.")
            _allTasks = []

            for _user, _attr in self.respUsers.items():
                _allTasks += self.client.getUserTasks(_attr['company_id'], _user)

            self.wrTasks.writerow(_allTasks)

        if 'worklogs' in self.paramEndpoints:
            logging.info("Downloading worklogs.")
            _allWorklogs = []

            for _comp in self.respCompanies:
                _allWorklogs += self.client.getWorklogs(_comp, self.paramDates['start'], self.paramDates['end'])

            self.wrWorklogs.writerow(_allWorklogs)

        if 'webandapp' in self.paramEndpoints:
            logging.info("Downloading websites and applications.")
            logging.warn("This action may take a long time and even result in 500 Internal Server Error.")
            _allWAA = []

            for _user, _attr in self.respUsers.items():

                for d in self.paramChunkDates:
                    logging.info(f"Downloading web and apps for user {_user} from {d['start_date']} to {d['end_date']}")
                    _allWAA += self.client.getWebAndApps(_attr['company_id'], d['start_date'],
                                                         d['end_date'], _user)

                self.wrWebAndApps.writerow(_allWAA)
