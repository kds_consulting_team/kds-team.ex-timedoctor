import csv
import json
import os
import hashlib

FIELDS_COMPANIES = ['company_id', 'company_name', 'company_time_zone_id',
                    'company_time_zone', 'company_subdomain', 'company_logo']
JSON_FIELDS_COMPANIES = []
PK_COMPANIES = ['company_id']

FIELDS_USERS = ['id', 'user_id', 'company_id', 'employee_id', 'full_name', 'first_name', 'last_name', 'email', 'url',
                'level', 'is_team_manager', 'managed_users', 'teams', 'new_user', 'team_status', 'payroll_access',
                'billing_access', 'avatar', 'screenshots_active', 'manual_time', 'permanent_tasks',
                'computer_time_popup', 'poor_time_popup', 'blur_screenshots', 'web_and_app_monitoring',
                'webcam_shots', 'screenshots_interval', 'user_role_value', 'status', 'reports_hidden',
                'time_tracking_started']
JSON_FIELDS_USERS = ['managed_users', 'teams']
PK_USERS = ['id', 'user_id']

FIELDS_PROJECTS = ['id', 'project_id', 'project_source', 'project_name', 'archived', 'default', 'url',
                   'is_limit_exceeded', 'project_time', 'project_time_in_seconds', 'users']
JSON_FIELDS_PROJECTS = ['users']
PK_PROJECTS = ['id', 'project_id']

FIELDS_TASKS = ['id', 'task_id', 'project_id', 'project_name', 'task_name', 'active', 'user_id', 'assigned_by',
                'url', 'task_link', 'status']
JSON_FIELDS_TASKS = []
PK_TASKS = ['id', 'task_id', 'user_id']

FIELDS_WORKLOGS = ['id', 'length', 'user_id', 'user_name', 'task_id', 'task_name', 'task_url', 'project_id',
                   'project_name', 'start_time', 'end_time', 'edited', 'last_modified', 'work_mode']
JSON_FIELDS_WORKLOGS = []
PK_WORKLOGS = ['id']

FIELDS_WEBANDAPPS = ['id', 'user_id', 'full_name', 'email', 'webandapps_name', 'webandapps_type']
PK_WEBANDAPPS = ['id']

FIELDS_WAAWORKLOGS = ['webandapps_id', 'worklog_id', 'duration']
PK_WAAWORKLOAGS = ['webandapps_id', 'worklog_id']


class TimeDoctorWriter:

    def __init__(self, tableName, incrementalFlag, dataPath='./data'):

        self.paramPath = dataPath
        self.paramTableName = tableName
        self.paramTable = tableName + '.csv'
        self.paramTablePath = os.path.join(self.paramPath, 'out/tables', self.paramTable)
        self.paramFields = eval(f'FIELDS_{tableName.upper()}')
        self.paramJsonFields = eval(f'JSON_FIELDS_{tableName.upper()}')
        self.paramPrimaryKey = eval(f'PK_{tableName.upper()}')
        self.paramIncremental = incrementalFlag

        self.createWriter()
        self.createManifest()

    def createManifest(self):

        template = {
            'incremental': self.paramIncremental,
            'primary_key': self.paramPrimaryKey
        }

        path = self.paramTablePath + '.manifest'

        with open(path, 'w') as manifest:

            json.dump(template, manifest)

    def createWriter(self):

        self.writer = csv.DictWriter(open(self.paramTablePath, 'w'), fieldnames=self.paramFields,
                                     restval='', extrasaction='ignore', quotechar='\"', quoting=csv.QUOTE_ALL)
        self.writer.writeheader()

    def writerow(self, listToWrite):

        for row in listToWrite:

            _dictToWrite = {}

            for key, value in row.items():

                if key in self.paramJsonFields:
                    _dictToWrite[key] = json.dumps(value)

                else:
                    _dictToWrite[key] = value

            self.writer.writerow(_dictToWrite)


class TimeDoctorWriterWAA:

    def __init__(self, incrementalFlag, dataPath='./data'):

        self.paramPath = os.path.join(dataPath, 'out/tables')
        self.varWaaPath = os.path.join(self.paramPath, 'websites-and-apps.csv')
        self.varWaaWlgPath = os.path.join(self.paramPath, 'websites-and-apps-worklogs.csv')
        self.paramIncremental = incrementalFlag

        self.createManifests()
        self.createWriters()

    def createManifests(self):

        with open(self.varWaaPath + '.manifest', 'w') as manWaa:

            json.dump({'incremental': self.paramIncremental, 'primary_key': PK_WEBANDAPPS}, manWaa)

        with open(self.varWaaWlgPath + '.manifest', 'w') as manWlg:

            json.dump({'incremental': self.paramIncremental, 'primary_key': PK_WAAWORKLOAGS}, manWlg)

    def createWriters(self):

        self.wrWaa = csv.DictWriter(open(self.varWaaPath, 'w'), FIELDS_WEBANDAPPS, restval='',
                                    extrasaction='ignore', quotechar='\"', quoting=csv.QUOTE_ALL)

        self.wrWaa.writeheader()

        self.wrWlg = csv.DictWriter(open(self.varWaaWlgPath, 'w'), FIELDS_WAAWORKLOGS, restval='',
                                    extrasaction='ignore', quotechar='\"', quoting=csv.QUOTE_ALL)
        self.wrWlg.writeheader()

    def writerow(self, listToWrite):

        for user in listToWrite:

            dUser = {
                'user_id': user['user_id'],
                'email': user['email'],
                'full_name': user['full_name']
            }

            for _waa in user['websites_and_apps']:

                dApp = {
                    'webandapps_name': _waa['name'],
                    'webandapps_type': _waa['timeType']
                }

                _waa_id = hashlib.md5('|'.join([str(dUser['user_id']), dApp['webandapps_name'],
                                                dApp['webandapps_type']]).encode()).hexdigest()

                self.wrWaa.writerow({'id': _waa_id, **dUser, **dApp})

                for wlg, value in _waa['worklog_ids'].items():

                    self.wrWlg.writerow({'webandapps_id': _waa_id, 'worklog_id': wlg, 'duration': value})
