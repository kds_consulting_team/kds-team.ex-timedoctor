**0.1.1**
Added documentation and sample configuration.

**0.1.0**
Changed logging level to information.
Fixed bug with writing tables.

**0.0.2**
Changed logging to debug.
Changes traceback limit to 3.

**0.0.1**
Initial testing version of the extractor.