# Time Doctor

The Time Doctor extractor for Keboola allows users to download data from Time Doctor about users and their activity. To download all data available from the API, the following prerequisities need to be met:

- authorize extractor with admin account.

## Configuration

A sample configuration, with both input and output folders can be found [in the component's repository](https://bitbucket.org/kds_consulting_team/kds-team.ex-timedoctor/src/master/component_config/sample-config/).

### Parameters

To configure the extractor, a set of objects which should be downloaded needs to be specified along with the date range, for which statistics table will be downloaded. A sample `config.json` file can be found [here](https://bitbucket.org/kds_consulting_team/kds-team.ex-timedoctor/src/master/component_config/sample-config/config.json).

#### Objects (`endpoints`)

A list of objects, which should be downloaded from the API. By design of the API, only two objects support date filtering, where specifying a valid date range will support only records present within the date range. Currently supported objects are:

- `companies`
    - **description:** downloads a list of companies, to which authenticated user has access to
    - **documentation:** https://webapi.timedoctor.com/doc#companies#get--v1.1-companies
    - **date:** no
- `users`
    - **description:** downloads a list of users present within the company
    - **documentation:** https://webapi.timedoctor.com/doc#users#get--v1.1-companies-{company_id}-users
    - **date:** no
- `projects`
    - **description:** downloads a list of projects and a list of users, who are assigned to each project
    - **documentation:** https://webapi.timedoctor.com/doc#projects#get--v1.1-companies-{company_id}-users-{user_id}-projects
    - **date:** no
- `tasks`
    - **description:** downloads a list of tasks which are assigned to each user
    - **documentation:** https://webapi.timedoctor.com/doc#tasks#get--v1.1-companies-{company_id}-users-{user_id}-tasks
    - **date:** no
- `worklogs`
    - **description:** downloads worklogs, to which project and task they belong to and how much time was logged
    - **documentation:** https://webapi.timedoctor.com/doc#worklogs#get--v1.1-companies-{company_id}-worklogs
    - **date:** yes
- `websites and applications`
    - **description:** downloads a list of websites and applications, where users spend time while working, and associated worklogs
    - **documentation:** https://webapi.timedoctor.com/doc#webandapp#get--v1.1-companies-{company_id}-webandapp
    - **date:** yes

#### Date settings (`date_settings`)

A start and end date for `worklogs` and `websites and applications` objects. Both start date and end date support relative date specification in format like `2 days ago`, `3 months ago`, `a year ago`, etc; or standard `YYYY-MM-DD` format can be used. For full list of supported formats visit [`dateparser` library documentation](https://dateparser.readthedocs.io/en/latest/).

### Output

All tables are loaded incrementally into storage with primary keys set.

## Development

For running the extractor locally, use following commands to build and run the container:

```
docker-compose build dev
docker-compose run --rm dev
```